const state = {
  uploadData: []
}
const mutations = {
  CHANGE_UPLOAD_DATA: (state, data) => {
    state.uploadData = data
  }
}

const actions = {
  setUploadData({ commit }, data) {
    commit('CHANGE_UPLOAD_DATA', data)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
