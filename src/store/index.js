import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import upload from './modules/upload'
Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    settings,
    upload
  },
  getters
})

export default store
