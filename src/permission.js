import router from './router'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import getPageTitle from '@/utils/get-page-title'
import { getWorkspaceId } from '@/api/upload'
import { setWorkspace, getWorkspace } from '@/utils/auth'

NProgress.configure({ showSpinner: false }) // NProgress Configuration

router.beforeEach(async (to, from, next) => {
  // start progress bar
  NProgress.start()

  // set page title
  document.title = getPageTitle(to.meta.title)
  if (!getWorkspace()) {
    const query = to.query
    const code = query.code
    if (code) {
      getWorkspaceId(code).then(res => {
        setWorkspace(res.body.workspace)
      })
    }
  }
  // determine whether the user has logged in
  next()
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
