const workspaceKey = 'workspace'
export function getWorkspace() {
  return sessionStorage.getItem(workspaceKey)
}

export function setWorkspace(workspace) {
  return sessionStorage.setItem(workspaceKey, workspace)
}

export function removeWorkspace() {
  return sessionStorage.removeItem(workspaceKey)
}
