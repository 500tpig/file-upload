import request from '@/utils/request'
const temp = '/api/v1.0'
// 文件上传
export function upload(data, cancelToken) {
  return request({
    url: temp + '/upload',
    method: 'post',
    isUpload: true,
    data,
    cancelToken // 需要设置cancelToken字段，用于取消请求使用
  })
}

// 获取WorkspaceId
export function getWorkspaceId(code) {
  return request({
    url: temp + `/admin/grant?code=${code}`,
    method: 'get'
  })
}

// 获取空间储存
export function workspace(params) {
  return request({
    url: temp + `/admin/workspace`,
    method: 'get',
    params
  })
}

// 获得文件储存列表
export function page(data) {
  return request({
    url: temp + '/admin/page',
    method: 'post',
    data
  })
}

// 批量删除
export function remove(data) {
  return request({
    url: temp + '/admin/remove',
    method: 'post',
    data
  })
}

// 修改名称
export function rename(data) {
  return request({
    url: temp + '/admin/rename',
    method: 'post',
    data
  })
}

// 判断是否存在
export function exist(data) {
  return request({
    url: temp + '/exist',
    method: 'post',
    data
  })
}
